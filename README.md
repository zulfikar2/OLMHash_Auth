# OLM_Hash

The basic idea for this project is to write a password scheme that
mimics that of LMHash (and NTLMHash v1) used in older Windows system.

Made by :
Zulfikar Yusufali
Joshua Colaco
Emmanuel Sinlao

You can compile using :

gcc -std=c99 main.c OLMHash.c OPC.c CRA.c Hasher.c -o main

and then run with :

./main

